import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Vector;

public class DataSink {
	
	InetAddress address;
	int port;
	private DatagramSocket runtimeSocket;
	private Vector <String> receivedPackets;
	private final int PACKETSIZE = 1024 ;


	DataSink(InetAddress address, int port)
	{
		this.address = address;
		this.port = port;
		receivedPackets = new Vector <String>();
		try
		{
			runtimeSocket = new DatagramSocket(port);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void receiveData()
	{
		System.out.println("Sink ready @ " + port);
		String receivedData = null;
		while(true)
	  	{
	  		try
	  		{
	  			DatagramPacket packet = new DatagramPacket( new byte[PACKETSIZE], PACKETSIZE );
	  			runtimeSocket.receive(packet);
	  			
	  			receivedData = new String(packet.getData()).trim();
	  			
	  			if(!receivedData.contains("Sending data"))
	  			{
	  				if(!receivedPackets.contains(receivedData))
	  				{
	  					receivedPackets.add(receivedData);
	  					System.out.println(receivedData + " From " + packet.getAddress());
	  				}
	  				
	  			}
	  				
	  		}
	  		catch(Exception e)
	  		{
	  			e.printStackTrace();
	  		}
	  	}
	}
	
	
	public static void main(String args[])
	{
		try
		{
			DataSink ds = new DataSink(InetAddress.getByName("localhost"), 2000);
			ds.receiveData();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	

}
