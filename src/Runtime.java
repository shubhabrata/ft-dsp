import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.JarURLConnection;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class Runtime extends ClassLoader{
	
	private InetAddress address;
	private int port;
	private DatagramSocket runtimeSocket;
	private final int PACKETSIZE = 1024 ;
	private Vector <String> destinationNodeInfo;
	private Vector<String> activeDataSources;
	private HashMap <String, Queue <String> >setOfBuffers;
	public BlockingQueue <String> inputBuffer;
	public BlockingQueue <String> outputBuffer;

	private Thread actorExecutorThread;
	public boolean inputBufferEmpty = true;
	public boolean outputBufferEmpty = true;
	public static int portNumber = 8000;
	private Class classToExecute;
	private Object actorInstance;

	Runtime(InetAddress address, int port)
	{
		this.address = address;
		this.port = port;
		destinationNodeInfo = new Vector <String>();
		activeDataSources = new Vector <String>();
		setOfBuffers = new HashMap <String, Queue <String> > ();
		inputBuffer = new LinkedBlockingQueue<String>();
		outputBuffer = new LinkedBlockingQueue<String>();

		try
		{
			InetAddress inteAddress = InetAddress.getByName("localhost");
		    SocketAddress addr = new InetSocketAddress(inteAddress, port);
			runtimeSocket = new DatagramSocket(null);
			runtimeSocket.bind(addr);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public Class getActorClass()
	{
		return this.classToExecute;
	}
	
	public Object getActorInstance()
	{
		return this.actorInstance;
	}
	
	public void waitForMessageFromOwner()
	{
		String receivedData = null;
	  	System.out.println("Runtime ready @ " + port);
	  	while(true)
	  	{
	  		try
	  		{
	  			DatagramPacket packet = new DatagramPacket( new byte[PACKETSIZE], PACKETSIZE );
	  			runtimeSocket.receive(packet);
	  			
	  			receivedData = new String(packet.getData()).trim();

	  			if( receivedData.startsWith("DATA-"))
	  			{
	  				System.out.println(receivedData + " FROM " + packet.getPort());
	  				handleDataStream(receivedData, packet.getAddress()+"");
	  			
	  			}
	  				
	  			if(receivedData.startsWith("DESTINATIONNODES"))
	  			{
	  				System.out.println(receivedData);
	  				String [] tokens = receivedData.split("DESTINATIONNODES");
	  				addDestinationNodes(tokens[1]);
	  			}
	  			
	  			if(receivedData.startsWith("SOURCENODES"))
	  			{
	  				System.out.println(receivedData);
	  				String [] tokens = receivedData.split("SOURCENODES");
	  				addSourceNode(tokens[1]);
	  			}
	  			
	  			if(receivedData.startsWith("Fetch codes"))
	  			{
	  				String[] tokens = receivedData.split("FROM");
	  				String actorLocation = tokens[1];
	  				System.out.println(actorLocation);
	  				fetchAndExecuteCode(actorLocation, Runtime.portNumber);
	  			}
	  				
	  				
	  		}
	  		catch(Exception e)
	  		{
	  			e.printStackTrace();
	  		}
	  	}
	}
	
	/*
	 * Add the destination ports where this source is supposed to send data
	 */
	public void addDestinationNodes(String destinationNodes)
	{
		String [] destinationNodesInfo = destinationNodes.split("END");
		for(int i = 0; i < destinationNodesInfo.length; i++)
		{
			if(destinationNodesInfo[i].contains("@"))
				destinationNodeInfo.add(destinationNodesInfo[i]);
		}
	}
	
	/*
	 * Add the information of the nodes that send data to this particular runtime
	 */
	public void addSourceNode(String sourceNode)
	{
		if(!activeDataSources.contains(sourceNode))
			activeDataSources.add(sourceNode);
		
		for(int i = 0; i < activeDataSources.size(); i++)
			System.out.println("Receiving data from " + activeDataSources.get(i));
	}
	
	/*
	 * Fetch and execute the actor code from a code repository
	 */
	public void fetchAndExecuteCode(String codeLocation, int port)
	{
		try
		{
			File file = new File(codeLocation);
			URL url = file.toURI().toURL();
	        URL[] urls = {url};
			ClassLoader classLoader = new URLClassLoader(urls);
			
			//Load the main class
			URL u = new URL("jar", "", url + "!/");
			JarURLConnection uc = (JarURLConnection)u.openConnection();
			Attributes attr = uc.getMainAttributes();
			String className = attr != null ? attr.getValue(Attributes.Name.MAIN_CLASS): null;
			classToExecute = classLoader.loadClass(className);
	
			
			//Uncomment later 
			Constructor constructor = classToExecute.getConstructor();
			actorInstance= constructor.newInstance();
		    
		  //  Method initMethod = classToExecute.getMethod("initialize", String[].class);
	   //     initMethod.invoke(actorInstance, new Object[] { new String[] { port+""} });
	        Runtime.portNumber++;
	//        ActorCommunicationManager actorCommunicator = new ActorCommunicationManager(classToExecute, actorInstance, this);
	 //       actorExecutorThread = new Thread(actorCommunicator);
	  //      actorExecutorThread.start();
	       
	        
	  //      Method connectionMethod = classToExecute.getMethod("handleConnection");
	  //      connectionMethod.invoke(actorInstance);
	        
	        
			
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Function to receive the incoming data streams
	 */
	
	public void handleDataStream(String receivedData, String address)
	{
		
			
		if(receivedData.equalsIgnoreCase("null"))
			return;
				
		addToBuffers(receivedData, address);
		System.out.println(receivedData);
		
	}
	
	
	/*
	 * Function to forward the data to the destination nodes
	 */
	
	public void sendToDestinationNodes(String data)
	{
		InetAddress address = null;
		int port = 0;
		for(int i = 0; i < destinationNodeInfo.size(); i++)
		{
			String addressKey = destinationNodeInfo.get(i);
			int index = addressKey.indexOf("@");
			try {
				address = InetAddress.getByName(addressKey.substring(index+1, addressKey.length()));
				port = Integer.parseInt(addressKey.substring(0,index));
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	
			System.out.println("Sending " + data);
			sendMessage(data, address, port);
		}
	}
	
	/*
	 * Generic function for sending messages to remote machines
	 */
	
	public void sendMessage(String message, InetAddress address, int port)
	{
		try
		{
			byte[] data = message.getBytes();
			
			DatagramPacket packet = new DatagramPacket( data, data.length, address, port );
			runtimeSocket.send(packet);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void addToBuffers(String data, String address)
	{
		if(data.equals("null"))
			return;
			
		String id = port +"@"+ address;
		
		if(!setOfBuffers.containsKey(id))
			setOfBuffers.put(id, new LinkedList <String>());
		
		setOfBuffers.get(id).add(data);
		
		sendToInputBuffer(id, data);
	}
	
	public void sendToInputBuffer(String id, String data)
	{
		synchronized(inputBuffer)
		{
			if(!inputBuffer.contains(data))
				inputBuffer.add(data);
		
			inputBufferEmpty = false;
			inputBuffer.notify();
		}
		
		//Send a message to the thread denoting that a new value has arrived
	}
	
	public void display()
	{
		for(String element : inputBuffer)
            System.out.println("Element : " + element);
        
	}
	
	public int getBufferSize()
	{
		return inputBuffer.size();
				
	}
	
	public String getQueueElement()
	{
		return inputBuffer.peek();
	}
	
	public void addToOutputBuffer(String value)
	{
		synchronized(this.outputBuffer)
		{
			if(!outputBuffer.contains(value))
				outputBuffer.add(value);
		
			outputBufferEmpty = false;
			outputBuffer.notify();
		}
	}
	
		
	public static void main(String args[])
	{
		try
		{
			Runtime runtime = new Runtime(InetAddress.getLocalHost(), Integer.parseInt(args[0]));
		//	new Thread(new RuntimeMonitoringThread(runtime)).start();
			
			new Thread(new ActorRuntimeInteractionThread(runtime)).start();
			new Thread(new DataTransmissionThread(runtime)).start();
			runtime.waitForMessageFromOwner();
			
	//		r.fetchAndExecuteCode("C:\\Users\\shubhabrata\\Dropbox\\test\\StreamImplementation\\src\\actor.jar");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}

/*
 * This thread is used to control the interaction between the actor
 * and the runtime
 */
class ActorRuntimeInteractionThread implements Runnable
{
	private Runtime runtime;
	
	ActorRuntimeInteractionThread(Runtime runtime)
	{
		this.runtime = runtime;
	}

	@Override
	public void run() {
		
		String value=null;
		String tokenReceivedFromActor = null;
		synchronized(runtime.inputBuffer)
		{
			while(runtime.inputBufferEmpty)
			{
				try {
					runtime.inputBuffer.wait();
					
					
					// Check with actor if it is ready for processing the token
					Method actorMethod = runtime.getActorClass().getMethod("checkActorStatus");
					
					if((boolean)actorMethod.invoke(runtime.getActorInstance()))
					{
						value = runtime.inputBuffer.remove();
						
						//Initiate the token processing operation with the actor by notifying the actor
						Method initateActorOperation = runtime.getActorClass().getMethod("processToken", new Class[]{String.class});
						tokenReceivedFromActor = initateActorOperation.invoke(runtime.getActorInstance(), new String(value))+"";
					}
						
					runtime.addToOutputBuffer(tokenReceivedFromActor);
					System.out.println("The value is " + tokenReceivedFromActor);
					
					runtime.inputBufferEmpty = true;
				} 
				
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
				catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
			}
			
			System.out.println("running");
			return;
		}
	}
	
}

/*
 * This thread monitors the output buffer and sends values from the output buffer to 
 * the destination nodes
 */
class DataTransmissionThread implements Runnable
{
	Runtime runtime;
	
	DataTransmissionThread(Runtime runtime)
	{
		this.runtime = runtime;
	}
	
	public void run()
	{
		synchronized(runtime.outputBuffer)
		{
			while(runtime.outputBufferEmpty)
			{
				try
				{
					runtime.outputBuffer.wait();
					String value = runtime.outputBuffer.remove();
					
					runtime.sendToDestinationNodes(value);
					
				//	if(runtime.outputBuffer.size() == 0)
						runtime.outputBufferEmpty = true;
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}

