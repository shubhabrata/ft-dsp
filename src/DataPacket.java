import java.io.Serializable;
import java.net.InetAddress;

public class DataPacket implements Serializable{
	
    private static final long serialVersionUID = 1L;
	private int port;
	private String message;
	public int token;
	private InetAddress address;
	
	DataPacket(int port, String message, int token, InetAddress address)
	{
		this.port = port;
		this.message = message;
		this.token = token;
		this.address = address;
		
	}
	
	public int getPort()
	{
		return port;
	}
	
	public String getmessage()
	{
		return message;
	}
	
	public int getToken()
	{
		return this.token;
	}
	
	public InetAddress getAddress()
	{
		return address;
	}
	
	public void incrementToken()
	{
		this.token++;
	}

}
