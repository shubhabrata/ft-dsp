package com.applicationOwner;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

public class ActorInformation implements Serializable{

	private String actorName;
	private boolean isReplica;
	private String parentActorName;
	private Vector <String> inputPorts;
	private Vector <String> outputPorts;
	private Hashtable <String, String> listOfConnections;
	
	private Class classToExecute;
	private Object actorInstance;
	private String actorCodeLocation;
	
	public ActorInformation()
	{
	
	}
	
	public void initialize(String actorName)
	{
		this.actorName = actorName;
		isReplica = false;
		inputPorts = new Vector <String> ();
		outputPorts = new Vector <String> ();
	}
	public void addActorCodeInformation(Class classToExecute, Object actorInstance, String actorCodeLocation)
	{
		this.classToExecute = classToExecute;
		this.actorInstance = actorInstance;
		this.actorCodeLocation = actorCodeLocation;
	}
	
	
	
	public void addInputPorts(String name)
	{
		inputPorts.add(name);
	}
	
	public void addOutputPorts(String name)
	{
		outputPorts.add(name);
	}
	
	public void addReplicaInformation(String parentActorName)
	{
		isReplica = true;
		this.parentActorName = parentActorName;
	}
	
	public Vector <String> getInputPorts()
	{
		return this.inputPorts;
	}
	
	public Vector <String> getOutputPorts()
	{
		return this.outputPorts;
	}
	
	public boolean getReplicaStatus()
	{
		return this.isReplica;
	}
	
	public String getParentActorName()
	{
		return this.parentActorName;
	}
	
	public void setParentActorInfo(boolean replica, String parentActor)
	{
		this.isReplica = replica;
		this.parentActorName = parentActor;
	}
	
	public void setIOBuffers(Vector <String> input, Vector <String> output)
	{
		this.inputPorts.addAll(input);
		this.outputPorts.addAll(output);
	}
	
	public Class getClassToExecute()
	{
		return this.classToExecute;
	}
	
	public Object getActorInstance()
	{
		return this.actorInstance;
	}
	
	public String getActorCodeLocation()
	{
		return this.actorCodeLocation;
	}
}
