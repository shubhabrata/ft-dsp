import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;


public class Actor {
	
	private boolean actorBusy; 
	private String processedToken;

	public Actor()
	{
		this.actorBusy = true;
		this.processedToken = null;
	}
	
	public String processToken(String tokenValue)
	{
		synchronized(this)
		{
			processedToken = tokenValue;
			processTokenFromRuntime(tokenValue);
			this.notify();
			return this.processedToken;
		}
		
	}
	
	/*
	 * This function is used by the runtime to check the status of an actor i.e. if that actor is ready to process a request or not
	 */
	public boolean checkActorStatus()
	{
		return actorBusy;
	}
	
	public int display()
	{
		System.out.println("Actor called from runtime");
		return 1;
	}
	
	/*
	 * This function signals to the main actor thread that the runtime is ready to send the token
	 */
	public void processTokenFromRuntime(String value)
	{
		synchronized(this)
		{
			System.out.println("Ready to start processing");
			this.processedToken = value + " added";
			
			this.notify();
			

			
		}
	}
	
	
	public static void main(String[] args) 
  	{
		Actor act = new Actor();
		
	//	act.initialize(args);
	//	DataBufferHelperThread helperThread = new DataBufferHelperThread(act);
	//	new Thread(helperThread).start();
	//	act.handleConnection();
	//	act.display();
		
  	}


	/*public void run() 
	{
				
	}*/
  	
	
}



