import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class DataSource {
	
	private int sourcePort;
	private DatagramSocket dataSourceSocket;
	private Vector <String>  destinationPorts = new Vector <String>();
	private final int PACKETSIZE = 1024 ;
	
	DataSource(String args[])
	{
		this.sourcePort = Integer.parseInt(args[0]);
		
		try
		{
			dataSourceSocket = new DatagramSocket(sourcePort);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void waitForMessageFromOwner()
	{
		try
		{
			String receivedData = null;
			System.out.println("Data source ready @ " + sourcePort);
			while(true)
			{
				DatagramPacket packet = new DatagramPacket( new byte[PACKETSIZE], PACKETSIZE );
				dataSourceSocket.receive(packet);
	  			
	  			receivedData = new String(packet.getData()).trim();
	  			
	  			if(receivedData.startsWith("DESTINATIONNODES"))
	  			{
	  				System.out.println(receivedData);
	  				String [] tokens = receivedData.split("DESTINATIONNODES");
	  				addDestinationNodes(tokens[1]);
	  			}
	  				
	  			
	  			if(receivedData.equalsIgnoreCase("Owner"))
	  				System.out.println("Received message from owner");
	  			if(receivedData.equalsIgnoreCase("Start transmission"))
	  				handleConnection();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Add the destination ports where this source is supposed to send data
	 */
	public void addDestinationNodes(String destinationNodes)
	{
		String [] destinationNodesInfo = destinationNodes.split("END");
		for(int i = 0; i < destinationNodesInfo.length; i++)
		{
			if(destinationNodesInfo[i].contains("@"))
				destinationPorts.add(destinationNodesInfo[i]);
		}
	}
	
	public void handleConnection()
	{
		System.out.println("Sending data");
		try 
		{
			
			ExecutorService executor = Executors.newFixedThreadPool(10);
			
			
			for (String destPort: destinationPorts)
			{
				int destinationPort = Integer.parseInt((destPort.split("@"))[0]);
				InetAddress host = InetAddress.getByName((destPort.split("@"))[1]);
				//new MessageSenderThread(sourcePort, destPort, host).run();
				Runnable worker = new MessageSenderThread(sourcePort, destinationPort, host);
				executor.execute(worker);
			}
				
			executor.shutdown();
			// Wait until all threads are finish
			while (!executor.isTerminated()) {
	 
			}
			
			
						
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	
	public static void main(String args[])
	{
		DataSource ds = new DataSource(args);
		ds.waitForMessageFromOwner();
		//ds.handleConnection();
	}
}


